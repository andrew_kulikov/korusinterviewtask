### Задание: ###
Формирование коротких ссылок

### Использовал: ###
* Java 11
* Spring
* Redis InMemory Server
* Junit + Mockito

### Дополнительно ###
Настроил сборку CI

### Результат ###
* Author name : Andrew Kulikov
* Codeship : [![Codeship Status for andrew_kulikov/korusinterviewtask](https://app.codeship.com/projects/14eac550-30d8-0138-a2ca-0e9bc902f752/status?branch=master)](https://app.codeship.com/projects/385364)

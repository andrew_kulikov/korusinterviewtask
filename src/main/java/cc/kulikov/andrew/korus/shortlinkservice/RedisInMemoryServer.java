package cc.kulikov.andrew.korus.shortlinkservice;


import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import redis.embedded.RedisServer;

@Component
public class RedisInMemoryServer {
        private RedisServer redisServer;

        public RedisInMemoryServer() throws IOException {
            this.redisServer = new RedisServer(9090);
        }

        @PostConstruct
        public void postConstruct() {
            redisServer.start();
        }

        @PreDestroy
        public void preDestroy() {
            redisServer.stop();
        }
}

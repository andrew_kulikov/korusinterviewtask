package cc.kulikov.andrew.korus.shortlinkservice;

public interface RedisRepository {
    boolean putShortLinkToFullLinkIfAbsent(final Url url);
    void putFullLink(final Url url);

    Url findUrl(Integer shortUrl);

    Url findFullUrl(String fullURL);
}
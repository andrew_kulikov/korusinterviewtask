package cc.kulikov.andrew.korus.shortlinkservice;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RedisRepositoryImpl implements RedisRepository {
    private static final String KEY = "url";
    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations<String, Object, Object> hashOperations;

    @Autowired
    public RedisRepositoryImpl(RedisTemplate<String, Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init() {
        hashOperations = redisTemplate.opsForHash();
    }

    public boolean putShortLinkToFullLinkIfAbsent(final Url url) {
        return hashOperations.putIfAbsent(KEY, url.getShortURL(), url.getFullURL());
    }

    public void putFullLink(final Url url) {
        hashOperations.put(KEY, url.getFullURL(), url.getShortURL());
    }

    public Url findUrl(final Integer shortURL) {
        String fullURL = (String) hashOperations.get(KEY, shortURL);
        if (fullURL == null) {
            return null;
        }
        return new Url(shortURL, fullURL);
    }

    public Url findFullUrl (String fullURL) {
        Integer shortURL = (Integer) hashOperations.get(KEY, fullURL);
        if (shortURL == null) {
            return null;
        }
        return new Url(shortURL, fullURL);
    }
}

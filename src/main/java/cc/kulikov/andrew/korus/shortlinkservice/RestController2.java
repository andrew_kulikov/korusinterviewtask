package cc.kulikov.andrew.korus.shortlinkservice;

import java.net.URI;
import java.util.Random;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestController2 {
    private final Supplier<Random> randomSupplier;
    private final RedisRepository redisRepository;

    @Autowired
    public RestController2(RedisRepository redisRepository, Supplier<Random> randomSupplier) {
        this.redisRepository = redisRepository;
        this.randomSupplier = randomSupplier;
    }

    @GetMapping("/short")
    public String makeShort(HttpServletRequest request, @RequestParam("url") String url) {
        Random random = randomSupplier.get();
        final String requestURL = request.getRequestURL().toString();
        final URI uri = URI.create(requestURL);

        final String fragment = uri.getScheme() + "://" + uri.getAuthority();

        int result = random.nextInt();

        if (redisRepository.findFullUrl(url) != null) {
            Url urls = redisRepository.findFullUrl(url);
            return fragment + "/" + urls.getShortURL();
        } else {
            String shortUrl = fragment + "/" + result;
            Url urls = new Url(result, url);
            while (!redisRepository.putShortLinkToFullLinkIfAbsent(urls)) {
                result = random.nextInt();
                urls = new Url(result, url);
            }
            redisRepository.putFullLink(urls);
            return shortUrl;
        }
    }

    @GetMapping("/full/{shortURL}")
    public ResponseEntity<String> returnFull(@PathVariable Integer shortURL) {
        final Url url = redisRepository.findUrl(shortURL);
        if (url != null) {
            return ResponseEntity.ok(url.getFullURL());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/{shortURL}")
    public ResponseEntity<Void> makeRedirect(@PathVariable Integer shortURL) {
        if (redisRepository.findUrl(shortURL) != null) {
            Url urls = redisRepository.findUrl(shortURL);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(urls.getFullURL()));
            return new ResponseEntity<>(headers, HttpStatus.MOVED_PERMANENTLY);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}

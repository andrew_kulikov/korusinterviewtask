package cc.kulikov.andrew.korus.shortlinkservice;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;

@RedisHash("url")
public class Url implements Serializable {
    private Integer shortURL;
    private String fullURL;

    public Url(Integer shortURL, String fullURL) {
        this.shortURL = shortURL;
        this.fullURL = fullURL;
    }

    public Integer getShortURL() {
        return shortURL;
    }

    public void setShortURL(Integer shortURL) {
        this.shortURL = shortURL;
    }

    public String getFullURL() {
        return fullURL;
    }

    public void setFullURL(String fullURL) {
        this.fullURL = fullURL;
    }
}

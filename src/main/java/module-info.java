module cc.kulikov.andrew.korus.shortlinkservice {
    requires com.fasterxml.classmate;

    //need for spring aop
    requires jdk.unsupported;

    requires embedded.redis;

    requires java.sql;

    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.web;
    requires spring.context;
    requires spring.data.redis;
    requires spring.beans;

    requires java.annotation;
    requires org.apache.tomcat.embed.core;

    requires com.fasterxml.jackson.databind;

    exports cc.kulikov.andrew.korus.shortlinkservice;
    opens cc.kulikov.andrew.korus.shortlinkservice to spring.core, spring.aop;
}
package cc.kulikov.andrew.korus.shortlinkservice;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Suppliers;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

// подключить springboot tests  добавить тесты на позитивные моменты
// добавить тест на отсутствующую короткую url (404)
// редирект 301

@SpringBootTest
public class RestController2Test {

    @Test
    public void make_short_url() {
        final RedisRepository redisRepository = Mockito.mock(RedisRepository.class);
        final HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(httpServletRequest.getRequestURL()).thenReturn(new StringBuffer("http://localhost:8080/short"));
        final Random mockRandom = Mockito.mock(Random.class);
        Mockito.when(mockRandom.nextInt()).thenReturn(1);

        final RestController2 restController2 = new RestController2(redisRepository, Suppliers.ofInstance(mockRandom));

        final Map<Object, Object> repoMap = new HashMap<>();

        Mockito.doAnswer(invocationOnMock -> {
            final Url argument = invocationOnMock.getArgument(0, Url.class);
            repoMap.put(argument.getShortURL(), argument.getFullURL());
            return true;
        }).when(redisRepository).putShortLinkToFullLinkIfAbsent(Mockito.any(Url.class));

        Mockito.doAnswer(invocationOnMock -> {
            final Url argument = invocationOnMock.getArgument(0, Url.class);
            repoMap.put(argument.getFullURL(), argument.getShortURL());
            return null;
        }).when(redisRepository).putFullLink(Mockito.any(Url.class));


        final String shortURL = restController2.makeShort(httpServletRequest, "http://vk.com");
        final URI uri = URI.create(shortURL);

        final String fragment = uri.getPath();
        final String substring = fragment.substring(1);

        Assert.assertEquals("1", substring);
    }

    @Test
    public void return_full_url() {
        RedisRepository redisRepository = Mockito.mock(RedisRepository.class);
        final Random mockRandom = Mockito.mock(Random.class);
        Mockito.when(mockRandom.nextInt()).thenReturn(1);

        final RestController2 restController2 = new RestController2(redisRepository, Suppliers.ofInstance(mockRandom));

        final Url url = new Url(1, "http://vk.com");
        Mockito.when(redisRepository.findUrl(Mockito.any(Integer.class))).thenReturn(url);

        final ResponseEntity<String> stringResponseEntity = restController2.returnFull(1);
        final String body = stringResponseEntity.getBody();
        Assert.assertEquals("http://vk.com", body);
    }

    @Test
    public void return_not_found_full_url() {
        final RedisRepository redisRepository = Mockito.mock(RedisRepository.class);
        final Random mockRandom = Mockito.mock(Random.class);
        Mockito.when(mockRandom.nextInt()).thenReturn(1);

        final RestController2 restController2 = new RestController2(redisRepository, Suppliers.ofInstance(mockRandom));

        Mockito.when(redisRepository.findUrl(Mockito.any(Integer.class))).thenReturn(null);

        final ResponseEntity<String> stringResponseEntity = restController2.returnFull(2);
        final HttpStatus statusCode = stringResponseEntity.getStatusCode();
        final int value = statusCode.value();
        Assert.assertEquals(404, value);
    }

    @Test
    public void redirect() {
        final RedisRepository redisRepository = Mockito.mock(RedisRepository.class);
        final Random mockRandom = Mockito.mock(Random.class);
        Mockito.when(mockRandom.nextInt()).thenReturn(1);

        final RestController2 restController2 = new RestController2(redisRepository, Suppliers.ofInstance(mockRandom));

        final Url url = new Url(1, "http://vk.com");
        Mockito.when(redisRepository.findUrl(Mockito.any(Integer.class))).thenReturn(url);

        final ResponseEntity<Void> voidResponseEntity = restController2.makeRedirect(1);
        final HttpStatus statusCode = voidResponseEntity.getStatusCode();
        final int value = statusCode.value();
        Assert.assertEquals(301, value);
    }

    @Test
    public void redirect_not_found() {
        RedisRepository redisRepository = Mockito.mock(RedisRepository.class);
        final Random mockRandom = Mockito.mock(Random.class);
        Mockito.when(mockRandom.nextInt()).thenReturn(1);

        final RestController2 restController2 = new RestController2(redisRepository, Suppliers.ofInstance(mockRandom));

        Mockito.when(redisRepository.findUrl(Mockito.any(Integer.class))).thenReturn(null);

        final ResponseEntity<Void> voidResponseEntity = restController2.makeRedirect(2);
        final HttpStatus statusCode = voidResponseEntity.getStatusCode();
        final int value = statusCode.value();
        Assert.assertEquals(404, value);
    }
}